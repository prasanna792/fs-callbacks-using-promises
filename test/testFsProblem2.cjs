const invoke = require('../fs-problem2.cjs');
const path = require('path');

const lipsumPath = path.join(__dirname, '../lipsum.txt');

invoke(lipsumPath)
.then((message) => {
    console.log(message);
})
.catch((err) => {
    console.error(err);
});