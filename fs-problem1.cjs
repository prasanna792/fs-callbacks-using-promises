

const fs = require('fs');
const path = require('path');
const randomFilesDirectory = path.join(__dirname, './randomFilesDirectory');

function createRandomDirectory() {
    return new Promise((resolve, reject) => {
        fs.mkdir(randomFilesDirectory, { recursive: true }, (err) => {
            if (err) {
                console.error(err);
                reject(err);
            } else {
                console.log("Created directory");
                resolve(); // Call resolve to indicate the successful completion of the asynchronous operation.
            }
        });
    });
}

function createFiles() {
    return new Promise((resolve, reject) => {
        const number = Math.floor(Math.random() * 10) + 1;
        const files = Array(number).fill(0).map((item, index) => {
            return `file${index + 1}.json`;
        });

        // console.log(files, "files");
        const create = files.map((item, index) => {
            // console.log(index,"index")
            return new Promise((resolve, reject) => {
                fs.writeFile(path.join(randomFilesDirectory, item), JSON.stringify({ name: "prasanna" }), (err) => {
                    if (err) {
                        console.error(err);
                        reject(err);
                    } else {
                        console.log(`created file ${index}`);
                        resolve();
                    }
                });
            });
        });

        Promise.all(create)
            .then(() => {
                resolve(files); // Call resolve here to indicate the successful completion of all the writeFile operations.
                console.log("All files created ");
            })
            .catch((err) => {
                reject(err);
            });
    });
}

function deleteRandomFiles(fileNames) {
    console.log(fileNames, "files");

    return new Promise(function (resolve, reject) {

        let checkList = fileNames.map((fileName) => {
            return deleteFile(fileName)
        })

        Promise.all(checkList)
            .then(() => {
                resolve()
                console.log("All Files are deleted successfully")
            })
            .catch(err => {
                reject(err)
            })
    })
}


function deleteFile(fileName) {

    return new Promise((resolve, reject) => {

        fs.unlink(path.join(randomFilesDirectory, fileName), (err) => {

            if (err) {
                reject(err)
            } else {
                console.log(`${fileName} is deleted`)
                resolve()
            }
        })
    })
}

function deleteDirectory() {
    return new Promise((resolve, reject) => {
        fs.rmdir(path.join(randomFilesDirectory), (err) => {
            if (err) {
                reject(err);
            } else {
                console.log("deleted created directory");
                resolve();
            }
        })
    })
}

function problem1() {

    return createRandomDirectory()
        .then(() => createFiles())
        .then((files) => deleteRandomFiles(files))
        .then(() => deleteDirectory())
        .catch((err) => console.log(err))
}

module.exports = problem1;




